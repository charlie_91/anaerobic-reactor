#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 21:44:17 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import statistics


# =============================================================================
# Calculating values for xmR chart
# =============================================================================
def x_mR_calc(x, feature):
    """

    Parameters
    ----------
    x : Pandas series
        Series containing measurements for the feature of interest.

    Returns
    -------
    data : Pandas dataframe
        Dataframe containing measurements and the moving range.

    """
    
    # Empty list for moving ranges
    mR = [np.nan]

    # Add moving ranges to list
    i = 1
    for data in range(1, len(x)):
        mR.append(abs(x[i] - x[i-1]))
        i += 1
        
    # Convert list to pandas Series  
    mR = pd.Series(mR)
        
    # Concatenate x and mR series and rename columns
    data = pd.concat([x,mR], axis=1).rename({feature:'x', 0:'mR'}, axis=1)
    
    return data



# =============================================================================
# Plotting xmR chart    
# =============================================================================
def x_mR_plot(data, feature, time):
    """

    Parameters
    ----------
    data : Pandas dataframe
        Dataframe containing measurements and the moving range.
    feature : string
        Name of the feature of interest.

    Returns
    -------
    None.

    """
    
    # Plot x and mR charts
    fig, axs = plt.subplots(2, figsize=(15,15))

    # x chart
    axs[0].plot(data['x'], 
                color='purple',
                marker='D',
                markerfacecolor='None',
                linestyle = 'None')
    
    axs[0].axhline(statistics.mean(data['x']), color='black', linewidth=2.0)
    axs[0].axhline(statistics.mean(data['x'])+3*statistics.mean(data['mR'][1:len(data['mR'])])/1.128, 
                   color = 'blue', 
                   linestyle = 'dashed', 
                   linewidth=2.0)
    axs[0].axhline(statistics.mean(data['x'])-3*statistics.mean(data['mR'][1:len(data['mR'])])/1.128, 
                   color = 'blue', 
                   linestyle = 'dashed', 
                   linewidth=2.0)
    # axs[0].set_title(str('x Chart for ' + variable_name), size=22)
    axs[0].set_xlabel('Time', size=22)
    axs[0].set_ylabel('Value of measurement', size=22)
    # axs[0].set_xticklabels(time, size=16)
    axs[0].set_xticks([x for x in range(0, len(time))])              # Location
    axs[0].set_xticklabels(time, fontsize=18, rotation=15)           # Display
    axs[0].tick_params(axis='x',bottom=True,length=5)
    axs[0].locator_params(axis='x', nbins=8)
    axs[0].yaxis.set_tick_params(labelsize=18)
    axs[0].grid()

    # mR chart
    axs[1].plot(data['mR'], 
                color='Purple',
                marker='D',
                markerfacecolor='None',
                linestyle='None')
    
    axs[1].axhline(statistics.mean(data['mR'][1:len(data['mR'])]), 
                   color='black', 
                   linewidth=2.0)
    
    axs[1].axhline(statistics.mean(data['mR'][1:len(data['mR'])])+3*statistics.mean(data['mR'][1:len(data['mR'])])*0.8525, 
                   color='blue', 
                   linestyle ='dashed', 
                   linewidth=2.0)
    
    axs[1].axhline(statistics.mean(data['mR'][1:len(data['mR'])])-3*statistics.mean(data['mR'][1:len(data['mR'])])*0.8525, 
                   color='blue', 
                   linestyle ='dashed', 
                   linewidth=2.0)
    
    axs[1].set_ylim(bottom=0)
    # axs[1].set_title(str('mR Chart for ' + feature), size=22)
    axs[1].set_xlabel('Time', size=22)
    axs[1].set_ylabel('Range', size=22)
    # axs[1].set_xticklabels(time, size=16)
    axs[1].set_xticks([x for x in range(0, len(time))])              # Location
    axs[1].set_xticklabels(time, fontsize=18, rotation=15)           # Display
    axs[1].tick_params(axis='x',bottom=True,length=5)
    axs[1].locator_params(axis='x', nbins=8)
    axs[1].yaxis.set_tick_params(labelsize=18)
    axs[1].grid()
    
    
    fig.savefig(feature + '_xmR.pdf', bbox_inches='tight')
    


# =============================================================================
# Counting measurements outside limits    
# =============================================================================
def x_mR_count(data, feature):
    """
    

    Parameters
    ----------
    data : Pandas dataframe
        Dataframe containing measurements and the moving range.
    feature : string
        Name of the feature of interest.

    Returns
    -------
    x_out : int
        Number of measurements outside control limits x chart.
    mR_out : int
        Number of measurements outside control limits mR chart.

    """
    x_out = 0
    for unit in data['x']:
        if unit > statistics.mean(data['x'])+3*statistics.mean(data['mR'][1:len(data['mR'])])/1.128 or unit < statistics.mean(data['x'])-3*statistics.mean(data['mR'][1:len(data['mR'])])/1.128:
            x_out += 1
    # print('Feature', feature, 'has', x_out, 'measurements outside of control limits for x chart.')        
    
    # Validate points out of control limits for mR chart
    mR_out = 0
    for unit in data['mR']:
        if unit > statistics.mean(data['mR'][1:len(data['mR'])])+3*statistics.mean(data['mR'][1:len(data['mR'])])*0.8525 or unit < statistics.mean(data['mR'][1:len(data['mR'])])-3*statistics.mean(data['mR'][1:len(data['mR'])])*0.8525:
            mR_out += 1
    # print('Feature', feature, 'has', mR_out, 'measurements outside of control limits for mR chart.')
    
    return x_out, mR_out

