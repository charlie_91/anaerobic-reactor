#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Apr  6 13:48:57 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt
# =============================================================================
# Data
# =============================================================================
process_tags = ['P-876-S', 'FI-20048', 'FFC-20070', 'FC-20779-MV', \
                'LIC-20032', 'QI-20475', 'QC-20476', 'FC-20402', 'QI-20775', \
                'QC-20774.A-MV', 'FC-20781-MV', 'TI-20774.B', 'LC-20103-MV', \
                'FT-20474A', 'TI-20453B', 'QC-20453A']
    
lab_tags = ['UM0207', 'UM0708', 'UM0206', 'UM0228', 'UM0233', 'UM0220', \
            'UM0227', 'UM0224', 'UM0235', 'UM0206_2', 'UM0807', 'UM0808', \
            'UM0815', 'UM0817', 'UM0818', 'UM0827', 'UM1441', 'UM1008']    



process_data = pd.read_csv('../Data/Prosessdata.csv', parse_dates=['Tid'], 
                           dayfirst=True, index_col = ['Tid'])
process_data.columns = process_tags
lab_data_or = pd.read_csv('../Data/Labdata.csv', parse_dates=['Tid'], 
                          dayfirst=True, index_col = ['Tid'])

# Removing empty rows and empty/duplicate columns
# -----------------------------------------------------------------------------
lab_data = lab_data_or.iloc[0:580,:]
lab_data.columns = lab_tags
lab_data = lab_data.drop(columns=['UM0224','UM0818', 'UM0206_2'])

# Defining new tags for later looping over the features
# -----------------------------------------------------------------------------
lab_tags_2 = ['UM0207', 'UM0708', 'UM0206', 'UM0228', 'UM0233', 'UM0220', \
              'UM0227', 'UM0235', 'UM0807', 'UM0808', 'UM0815', \
              'UM0817', 'UM0827', 'UM1441', 'UM1008']
    
# Seperating out the time for later plotting
# -----------------------------------------------------------------------------
process_time = process_data.index
lab_time = lab_data.index

# =============================================================================
# Imputing missing values
# =============================================================================
"""
Replacing missing values with methods in Pandas.
Replacing missing values with the mean value for the given feature. 
"""
for tag in process_tags:
    process_data[tag].fillna(process_data[tag].mean(), inplace = True)
    
for tag in lab_tags_2:
    lab_data[tag].fillna(lab_data[tag].mean(), inplace=True)


# =============================================================================
# Separating
# =============================================================================
"""
Process data
"""
# Into months
process_months = [g for n, g in process_data.groupby(pd.Grouper(freq='M'))]
# Into weeks
process_weeks = [g for n, g in process_data.groupby(pd.Grouper(freq='W'))]

"""
Lab data
"""
# Into months
lab_months = [g for n, g in lab_data.groupby(pd.Grouper(freq='M'))]
# Into weeks
lab_weeks = [g for n, g in lab_data.groupby(pd.Grouper(freq='W'))]


# =============================================================================
# Standardizing
# =============================================================================
"""
Standardizing the data using StandardScaler from scikit-learn. 
Creating a new dataframe for the standardized data. 
"""
sc = StandardScaler()

process_data_std = sc.fit_transform(process_data)
process_std = pd.DataFrame(process_data_std, columns=process_tags, 
                           index=process_data.index)

lab_data_std = sc.fit_transform(lab_data)
lab_std = pd.DataFrame(lab_data_std, columns=lab_tags_2,
                       index=lab_data.index)


# =============================================================================
# Process daily
# =============================================================================
"""
Accumulating the hourly process data into daily data by summing up the
values of the measurements. Done for original and standardized data.
"""
process_daily = process_data.resample('D').sum()
process_daily_std = process_std.resample('D').sum()
process_time_daily = process_daily.index 


# =============================================================================
# EWMA    
# =============================================================================
"""
A function calculating the EWMA value.
The code is based on the clas ewma created by: Carlos Henrique Silva
https://github.com/carlosqsilva/pyspc/tree/master/pyspc
"""
    
def ewma(data, feature, time, weight=0.2):
    """
    

    Parameters
    ----------
    data : Array
        Array with the measurements.
    time : Array
        Array with the date of measurements.
    feature : Str
        Name of feature.
    weight : Float, optional
        Weighted factor between 0 and 1. The default is 0.2.

    Returns
    -------
    None.

    """
    
    target = np.mean(data)

    # calculate the standard deviation
    rbar = []
    for i in range(len(data) - 1):
        rbar.append(abs(data[i] - data[i + 1]))
    std = np.mean(rbar) / 1.128
    
    # calculate ewma values
    ewma = []  
    i = target
    for x in data:
        ewma.append(weight * x + (1 - weight) * i)
        i = ewma[-1]

    lcl, ucl = [], []
    for i in range(1, len(data) + 1):
        lcl.append(target - 3 * (std) * np.sqrt((weight / (2 - weight)) * (1 - (1 - weight)**(2 * i))))
        ucl.append(target + 3 * (std) * np.sqrt((weight / (2 - weight)) * (1 - (1 - weight)**(2 * i))))

    fig, ax = plt.subplots(1, figsize=(12, 7))
    ax.plot([0, len(ewma)], [target, target], 'k-')
    ax.plot(lcl, 'b:')
    ax.plot(ucl, 'b:')
    ax.plot(ewma, 'mo')
    ax.set_xlabel('Time', size=20)
    ax.set_ylabel('EWMA value', size=20)
    ax.set_xticks([x for x in range(0, len(time))])              # Location
    ax.set_xticklabels(time, fontsize=18, rotation=15)           # Display
    ax.tick_params(axis='x',bottom=True,length=5)
    ax.locator_params(axis='x', nbins=8)
    ax.yaxis.set_tick_params(labelsize=18)
    # ax.set_yticklabels(ax.get_yticklabels(), rotation = 0, fontsize = 15) 
    # ax.set_title(str('EWMA Chart for ' + self._feature), size=22)
    
    ax.grid()
    fig.savefig(feature + '_ewma.pdf', bbox_inches='tight')
    

    
# =============================================================================
# Contruct charts  
# =============================================================================
# Process data
# Original
for feature in process_daily:
    data = process_daily.loc[:, feature].values
    ewma(data, feature, process_daily.index.date)

# Standardized
for feature in process_daily_std:
    data = process_daily_std.loc[:, feature].values
    ewma(data, feature, process_daily.index.date)
    
# Lab data
# Original
for feature in lab_data:
    data = lab_data.loc[:, feature].values
    ewma(data, feature, lab_data.index.date)

# Standadized
for feature in lab_std:
    data = lab_std.loc[:, feature].values
    ewma(data, feature, lab_std.index.date)

