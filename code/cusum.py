#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar 11 21:42:52 2021

@author: charlottolofsson
"""

# =============================================================================
# Modules
# =============================================================================
import pandas as pd
import numpy as np
from sklearn.preprocessing import StandardScaler
import matplotlib.pyplot as plt

from pyspc import spc
# =============================================================================
# Data
# =============================================================================
process_tags = ['P-876-S', 'FI-20048', 'FFC-20070', 'FC-20779-MV', \
                'LIC-20032', 'QI-20475', 'QC-20476', 'FC-20402', 'QI-20775', \
                'QC-20774.A-MV', 'FC-20781-MV', 'TI-20774.B', 'LC-20103-MV', \
                'FT-20474A', 'TI-20453B', 'QC-20453A']
    
lab_tags = ['UM0207', 'UM0708', 'UM0206', 'UM0228', 'UM0233', 'UM0220', \
            'UM0227', 'UM0224', 'UM0235', 'UM0206_2', 'UM0807', 'UM0808', \
            'UM0815', 'UM0817', 'UM0818', 'UM0827', 'UM1441', 'UM1008']    



process_data = pd.read_csv('../Data/Prosessdata.csv', parse_dates=['Tid'], 
                           dayfirst=True, index_col = ['Tid'])
process_data.columns = process_tags
lab_data_or = pd.read_csv('../Data/Labdata.csv', parse_dates=['Tid'], 
                          dayfirst=True, index_col = ['Tid'])

# Removing empty rows and empty/duplicate columns
# -----------------------------------------------------------------------------
lab_data = lab_data_or.iloc[0:580,:]
lab_data.columns = lab_tags
lab_data = lab_data.drop(columns=['UM0224','UM0818', 'UM0206_2'])

# Defining new tags for later looping over the features
# -----------------------------------------------------------------------------
lab_tags_2 = ['UM0207', 'UM0708', 'UM0206', 'UM0228', 'UM0233', 'UM0220', \
              'UM0227', 'UM0235', 'UM0807', 'UM0808', 'UM0815', \
              'UM0817', 'UM0827', 'UM1441', 'UM1008']
    
# Seperating out the time for later plotting
# -----------------------------------------------------------------------------
process_time = process_data.index
lab_time = lab_data.index

# =============================================================================
# Imputing missing values
# =============================================================================
"""
Replacing missing values with methods in Pandas.
Replacing missing values with the mean value for the given feature. 
"""
for tag in process_tags:
    process_data[tag].fillna(process_data[tag].mean(), inplace = True)
    
for tag in lab_tags_2:
    lab_data[tag].fillna(lab_data[tag].mean(), inplace=True)


# =============================================================================
# Separating
# =============================================================================
"""
Process data
"""
# Into months
process_months = [g for n, g in process_data.groupby(pd.Grouper(freq='M'))]
# Into weeks
process_weeks = [g for n, g in process_data.groupby(pd.Grouper(freq='W'))]

"""
Lab data
"""
# Into months
lab_months = [g for n, g in lab_data.groupby(pd.Grouper(freq='M'))]
# Into weeks
lab_weeks = [g for n, g in lab_data.groupby(pd.Grouper(freq='W'))]


# =============================================================================
# Standardizing
# =============================================================================
"""
Standardizing the data using StandardScaler from scikit-learn. 
Creating a new dataframe for the standardized data. 
"""
sc = StandardScaler()

process_data_std = sc.fit_transform(process_data)
process_std = pd.DataFrame(process_data_std, columns=process_tags, 
                           index=process_data.index)

lab_data_std = sc.fit_transform(lab_data)
lab_std = pd.DataFrame(lab_data_std, columns=lab_tags_2,
                       index=lab_data.index)


# =============================================================================
# Process daily
# =============================================================================
"""
Accumulating the hourly process data into daily data by summing up the
values of the measurements. Done for original and standardized data.
"""
process_daily = process_data.resample('D').sum()
process_daily_std = process_std.resample('D').sum()
process_time_daily = process_daily.index 


# =============================================================================
# CUSUM
# =============================================================================
"""
Class cchart and cusum created by: Carlos Henrique Silva
https://github.com/carlosqsilva/pyspc/tree/master/pyspc
"""

d2 = [0, 0, 1.128, 1.693, 2.059, 2.326, 2.534, 2.704, 2.847, 2.970, 3.078]

class ccharts(object):

    def __init__(self):
        self.layers = [self]

    def __radd__(self, model):
        if isinstance(model, spc):
            model.layers += self.layers
            return model

        self.layers.append(model)
        return self



class cusum(ccharts):

    _title = "CUSUM Chart"

    def __init__(self, target=None, std=None, interval=4, feature=None, time=None):
        super(cusum, self).__init__()

        self.target = target
        self.std = std
        self.interval = interval
        self._feature = feature
        self._time = time

    def plot(self, data, size, newdata=None):

        if size > 1:
            data = np.mean(data, axis=1)

        target = self.target
        std = self.std
        interval = self.interval

        if target is None:
            target = np.mean(data)

        if std is None:
            rbar = []
            for i in range(len(data) - 1):
                rbar.append(abs(data[i] - data[i + 1]))
            std = np.mean(rbar) / d2[2]

        k = std / 2

        cplus = []  # values
        cminus = []  # values
        i, j = 0, 0
        for xi in data:
            cplus.append(max([0, xi - (target + k) + i]))
            cminus.append(min([0, xi - (target - k) + j]))
            i, j = cplus[-1], cminus[-1]

        lcl = -interval * std
        ucl = interval * std
        center = 0
        
        fig, ax = plt.subplots(1, figsize=(12, 7))
        ax.plot([0, len(cplus)], [center, center], 'k-')


        ax.plot([0, len(cplus)], [lcl, lcl], 'b:')
        ax.plot([0, len(cplus)], [ucl, ucl], 'b:')
        ax.plot(cplus, 'mo')
        ax.plot(cminus, 'mo')
        ax.set_xlabel('Time [date]', size=22)
        ax.set_ylabel('CUSUM value', size=22)
        ax.set_xticklabels(self._time, size=9)
        ax.set_title(str('CUSUM Chart for ' + self._feature), size=22)
        # fig.suptitle(self._feature, fontsize=22)
        
        fig.savefig(self._feature + '.pdf', bbox_inches='tight')

#        ax.plot([0, len(cplus)], [center, center], 'k-')
#        ax.plot([0, len(cplus)], [lcl, lcl], 'r:')
#        ax.plot([0, len(cplus)], [ucl, ucl], 'r:')
#        ax.plot(cplus, 'bo--')
#        ax.plot(cminus, 'bo--')

        return ([cplus, cminus], center, lcl, ucl, self._title)
    
    
# =============================================================================
# Contruct charts  
# =============================================================================
# Process data
for feature in process_daily_std:
    data = process_daily_std.loc[:, feature]
    chart = spc(data) + cusum(feature=feature, time=process_time_daily)
    print(chart)
    
# Lab data
for feature in lab_data:
    data = lab_data.loc[:, feature]
    chart = spc(data) + cusum(feature=feature, time=lab_time)
    print(chart)
    