#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  3 16:06:06 2021

@author: charlottolofsson
"""


# =============================================================================
# Modules
# =============================================================================
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import mean_squared_error

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import statistics

from x_mR import x_mR_calc, x_mR_plot, x_mR_count
# =============================================================================
# Data
# =============================================================================
process_tags = ['P-876-S', 'FI-20048', 'FFC-20070', 'FC-20779-MV', \
                'LIC-20032', 'QI-20475', 'QC-20476', 'FC-20402', 'QI-20775', \
                'QC-20774.A-MV', 'FC-20781-MV', 'TI-20774.B', 'LC-20103-MV', \
                'FT-20474A', 'TI-20453B', 'QC-20453A']
    
lab_tags = ['UM0207', 'UM0708', 'UM0206', 'UM0228', 'UM0233', 'UM0220', \
            'UM0227', 'UM0224', 'UM0235', 'UM0206_2', 'UM0807', 'UM0808', \
            'UM0815', 'UM0817', 'UM0818', 'UM0827', 'UM1441', 'UM1008']    



process_data = pd.read_csv('../Data/Prosessdata.csv', parse_dates=['Tid'], 
                           dayfirst=True, index_col = ['Tid'])
process_data.columns = process_tags
lab_data_or = pd.read_csv('../Data/Labdata.csv', parse_dates=['Tid'], 
                          dayfirst=True, index_col = ['Tid'])

# Removing empty rows and empty/duplicate columns
# -----------------------------------------------------------------------------
lab_data = lab_data_or.iloc[0:580,:]
lab_data.columns = lab_tags
lab_data = lab_data.drop(columns=['UM0224','UM0818', 'UM0206_2', 'UM0708'])

# Defining new tags for later looping
# -----------------------------------------------------------------------------
lab_tags_2 = ['UM0207', 'UM0206', 'UM0228', 'UM0233', 'UM0220', \
              'UM0227', 'UM0235', 'UM0807', 'UM0808', 'UM0815', \
              'UM0817', 'UM0827', 'UM1441', 'UM1008']

# =============================================================================
# Imputing missing values
# =============================================================================
"""
Replacing missing values with methods in Pandas.
Replacing missing values with the mean value for the given feature. 
"""

for tag in process_tags:
    process_data[tag].fillna(process_data[tag].mean(), inplace = True)
    
for tag in lab_tags_2:
    lab_data[tag].fillna(lab_data[tag].mean(), inplace=True)


# =============================================================================
# Separating
# =============================================================================
"""
Process data
"""
# Into months
process_months = [g for n, g in process_data.groupby(pd.Grouper(freq='M'))]
# Into weeks
process_weeks = [g for n, g in process_data.groupby(pd.Grouper(freq='W'))]

"""
Lab data
"""
# Into months
lab_months = [g for n, g in lab_data.groupby(pd.Grouper(freq='M'))]
# Into weeks
lab_weeks = [g for n, g in lab_data.groupby(pd.Grouper(freq='W'))]


# =============================================================================
# Standardizing
# =============================================================================
"""
Standardizing the data using StandardScaler from scikit-learn. 
Creating a new dataframe for the standardized data. 
"""
sc = StandardScaler()

process_data_std = sc.fit_transform(process_data)
process_std = pd.DataFrame(process_data_std, columns=process_tags,
                           index=process_data.index)

lab_data_std = sc.fit_transform(lab_data)
lab_std = pd.DataFrame(lab_data_std, columns=lab_tags_2, 
                       index=lab_data.index)


# =============================================================================
# Correlation heatmap
# =============================================================================
"""
Finding correlation between features and plotting in heatmaps. 
"""
correlation_process = process_data.corr()
correlation_lab = lab_data.corr()

fig, ax = plt.subplots(figsize=(20,10))
# plt.title('Correlations Process Data')
sns.heatmap(correlation_process,
            vmin=-1,
            vmax=1,
            cmap="YlGnBu",
            cbar=True,
            annot=True,
            square=True,
            fmt='.2f')
ax.set_xticklabels(ax.get_yticklabels(), rotation = 30, fontsize = 15)
ax.set_yticklabels(ax.get_yticklabels(), rotation = 0, fontsize = 15)
# fig.savefig('correlation_process_data_blue.pdf')


fig, ax = plt.subplots(figsize=(20,10))
# plt.title('Correlations Labdata')
sns.heatmap(correlation_lab,
            vmin=-1,
            vmax=1,
            cmap="YlGnBu",
            cbar=True,
            annot=True,
            square=True,
            fmt='.2f')
ax.set_xticklabels(ax.get_yticklabels(), rotation = 30, fontsize = 15)
ax.set_yticklabels(ax.get_yticklabels(), rotation = 0, fontsize = 15) 
fig.savefig('correlations_lab_blue.pdf')



# =============================================================================
# Daily process values
# =============================================================================
"""
Accumulating the hourly process data into daily data by summing up the
values of the measurements. Done for original and standardized data.
"""
process_daily = process_data.resample('D').sum()
process_daily_std = process_std.resample('D').sum()


# =============================================================================
# Comparison
# =============================================================================
# Measured gas
measured = lab_data.iloc[30:, 12]
meas_calc = measured.to_frame()
meas_calc.columns = ['Measured']

# Calculating theoretical amount of gas
calculated = []

amount_to_reactor = process_daily.iloc[:, 10].values
cod_in = lab_data.iloc[30:, 13].values
cod_out = lab_data.iloc[30:, 8].values

for i in range(len(measured)):
    calculated_gas = (amount_to_reactor[i]*(cod_in[i]-cod_out[i])*0.001512)
    calculated.append(calculated_gas)
        
meas_calc['Calculated'] = calculated   


# =============================================================================
# RMSE
# =============================================================================
measured_array = meas_calc['Measured'].values
calculated_array = meas_calc['Calculated'].values
rmse = mean_squared_error(measured_array, calculated_array, squared=False)

mean_measured = np.mean(measured_array)
mean_calculated = np.mean(calculated_array)


# =============================================================================
# Standardizing
# =============================================================================
sc = StandardScaler()
meas_calc_std = pd.DataFrame(sc.fit_transform(meas_calc), columns=['Measured',
                                                                  'Calculated'])
rmse = mean_squared_error(meas_calc_std['Measured'], 
                          meas_calc_std['Calculated'],squared=False)


# =============================================================================
# Plotting
# =============================================================================
# =============================================================================
# All the data
# =============================================================================
x = meas_calc.index.date
fig,ax = plt.subplots(1, figsize=(15, 10))
plt.plot(x, meas_calc['Measured'], label = 'Measured gas', 
         linewidth=1.5)  
plt.plot(x, meas_calc['Calculated'], label = 'Predicted COD to gas', 
         linewidth=1.5)


# ax.set_xticklabels(fontsize=20, rotation=15) 
ax.yaxis.set_tick_params(labelsize=20)
ax.xaxis.set_tick_params(labelsize=20, rotation=15)
start, end = ax.get_xlim()
ax.xaxis.set_ticks(np.arange(start, end, 50))

ax.set_ylabel('Amount of gas per day [cubicmeter]', fontsize=20)
ax.set_xlabel('Time', fontsize=20)
ax.legend(fontsize=25)
#ax.set_title('Comparing measured and theoretical maximum value for raw gas', 
#             fontsize=22)
plt.grid()
plt.show()
fig.savefig('comparing_all.pdf', bbox_inches='tight') 

"""
ax.set_xticks([x for x in range(0, len(time))])              # Location
ax.set_xticklabels(time, fontsize=18, rotation=15)           # Display
ax.tick_params(axis='x',bottom=True,length=5)
ax.locator_params(axis='x', nbins=8)
ax.yaxis.set_tick_params(labelsize=18)

"""

# =============================================================================
# First 6 months
# =============================================================================
meas_calc1 = meas_calc.copy(deep=True)
meas_calc1 = meas_calc1.iloc[:184, :]
x = meas_calc1.index.date

fig,ax = plt.subplots(1, figsize=(15, 10))
plt.plot(x, meas_calc1['Measured'], label = 'Measured gas', 
         linewidth=3.0)  
plt.plot(x, meas_calc1['Calculated'], label = 'Predicted COD to gas', 
         linewidth=3.0)


# ax.set_xticklabels(fontsize=20, rotation=15) 
ax.yaxis.set_tick_params(labelsize=20)
ax.xaxis.set_tick_params(labelsize=20, rotation=15)
start, end = ax.get_xlim()
ax.xaxis.set_ticks(np.arange(start, end, 20))

ax.set_ylabel('Amount of gas per day [cubicmeter]', fontsize=20)
ax.set_xlabel('Time', fontsize=20)
ax.legend(fontsize=25)
# ax.set_title('Comparing measured and theoretical maximum value for raw gas', fontsize=22)
plt.grid()
plt.show()
fig.savefig('comparing_6_months.pdf', bbox_inches='tight') 


# =============================================================================
# First 6 months standardized
# =============================================================================
meas_calc1 = meas_calc_std.copy(deep=True)
meas_calc1 = meas_calc1.iloc[:184, :]

fig,ax = plt.subplots(1, figsize=(15, 10))
plt.plot(meas_calc1.index, meas_calc1['Measured'], label = 'Measured gas', 
         linewidth=3.0)  
plt.plot(meas_calc1.index, meas_calc1['Calculated'], label = 'Predicted COD gas', 
         linewidth=3.0)

ax.yaxis.set_tick_params(labelsize=20)
ax.xaxis.set_tick_params(labelsize=20)
ax.set_ylabel('Amount of gas per day [cubicmeter]', fontsize=20)
ax.set_xlabel('Time', fontsize=20)
ax.legend(fontsize=25)
# ax.set_title('Comparing measured and theoretical maximum value for raw gas', fontsize=22)
# fig.savefig('comparing_6_months.pdf', bbox_inches='tight') 


# =============================================================================
# xmR
# =============================================================================
"""
Looping through the different features in the process data to count the number
of measurements outside of the control limits. The number is stored in lists. 
This is done four times for process data:
    1. The original data.
    2. Original data accumulated to daily values.
    3. Standardized data.
    4. Standardized data accumulatedd
For the lab data this is done two times:
    1. The original data.
    2. Standardized data.
Also plotting the xmR charts for the standardized data. 
"""

# Original measurements
# -----------------------------------------------------------------------------
x = []
mR = []
x_acc = []
mR_acc = []

# Hourly 
for feature in process_data:
    x_data = pd.Series(process_data.loc[:, feature])
    x_data = x_data.reset_index(drop=True)
    x_mR_data = x_mR_calc(x_data, feature)
    x_count, mR_count = x_mR_count(x_mR_data, feature)
    x.append(x_count)
    mR.append(mR_count)
    x_mR_plot(x_mR_data, feature, process_std.index.date)
    

# Daily   
for feature in process_daily:
    x_data = pd.Series(process_daily.loc[:, feature])
    x_data = x_data.reset_index(drop=True)
    x_mR_data = x_mR_calc(x_data, feature)
    x_count, mR_count = x_mR_count(x_mR_data, feature)
    x_acc.append(x_count)
    mR_acc.append(mR_count)
    x_mR_plot(x_mR_data, feature, process_daily.index.date)
    
    
# Creating dataframe with the counts 
process_counts = pd.DataFrame(list(zip(x, mR, x_acc, mR_acc)),
                              columns = ['x', 'mR', 'x_acc', 'mR_acc'], 
                              index = process_tags)
process_counts.to_csv('process_counts.csv')
    


# Standardized data
# -----------------------------------------------------------------------------
x = []
mR = []
x_acc = []
mR_acc = []


# Hourly
for feature in process_std:
    x_data = pd.Series(process_std.loc[:, feature])
    x_data = x_data.reset_index(drop=True)
    x_mR_data = x_mR_calc(x_data, feature)
    x_count, mR_count = x_mR_count(x_mR_data, feature)
    x.append(x_count)
    mR.append(mR_count)
    x_mR_plot(x_mR_data, feature, process_std.index.date)
    
  
# Daily
for feature in process_daily_std:
    x_data = pd.Series(process_daily_std.loc[:, feature])
    x_data = x_data.reset_index(drop=True)
    x_mR_data = x_mR_calc(x_data, feature)
    x_count, mR_count = x_mR_count(x_mR_data, feature)
    x_acc.append(x_count)
    mR_acc.append(mR_count)
    x_mR_plot(x_mR_data, feature, process_daily_std.index.date)
    

# Creating dataframe with the counts  
process_counts_std = pd.DataFrame(list(zip(x, mR, x_acc, mR_acc)),
                                  columns = ['x', 'mR', 'x_acc', 'mR_acc'], 
                                  index = process_tags)
process_counts_std.to_csv('process_counts_std.csv')



# Labdata
# -----------------------------------------------------------------------------
# Original data
x = []
mR = []


for feature in lab_data:
    x_data = pd.Series(lab_data.loc[:, feature])
    x_data = x_data.reset_index(drop=True)
    x_mR_data = x_mR_calc(x_data, feature)
    x_count, mR_count = x_mR_count(x_mR_data, feature)
    x.append(x_count)
    mR.append(mR_count)
    x_mR_plot(x_mR_data, feature, lab_data.index.date)

 
# Creating dataframe with the counts    
lab_counts = pd.DataFrame(list(zip(x, mR)),
                          columns = ['x', 'mR'],
                          index=lab_tags_2)  
lab_counts.to_csv('lab_counts.csv')


# Standardized data
x = []
mR = []


for feature in lab_std:
    x_data = pd.Series(lab_std.loc[:, feature])
    x_data = x_data.reset_index(drop=True)
    x_mR_data = x_mR_calc(x_data, feature)
    x_count, mR_count = x_mR_count(x_mR_data, feature)
    x.append(x_count)
    mR.append(mR_count)
    x_mR_plot(x_mR_data, feature, lab_std.index.date)


# Creating dataframe with the counts     
lab_counts_std = pd.DataFrame(list(zip(x, mR)),
                              columns = ['x', 'mR'],
                              index=lab_tags_2)  
lab_counts_std.to_csv('lab_counts_std.csv')



# =============================================================================
# Abnormal measurements
# =============================================================================
"""
Looping through the features and finding measurements that are more than +- 3
times sigma from the mean value. Storing the mesurements that are abonormal
as there original value and setting the measurements that are "normal" to None.
This in order to plot only the abnormal measurements. 
"""

X = []

for feature in process_daily_std:
    x = []
    mean = np.mean(process_daily_std[feature].to_numpy())
    std = np.std(process_daily_std[feature].to_numpy())
    ucl = mean + (3 * std)
    lcl = mean - (3 * std)
    for meas in process_daily_std[feature]:
        if meas > ucl or meas < lcl:
            x.append(meas)                   # Abnormal
        else:
            x.append(None)                   # Normal
    X.append(x)
    

# Creating dataframe    
process_daily_std_out = pd.DataFrame(X)
process_daily_std_out = pd.DataFrame(process_daily_std_out.T.values, 
                                     columns=process_tags, 
                                     index=process_daily.index)  


# Plotting 
x = process_daily_std_out.values
y = process_daily_std_out.index.date
colors = ['tab:blue', 'tab:orange', 'tab:green', 'tab:red', 'tab:purple', 
          'tab:brown', 'tab:pink', 'tab:greay', 'tab:olive', 'tab:cyan']
markers = ['s', 'x', '^', '<', 'x', 'o', '>', 'X', 'v', 'd', 'o', \
           's', 'x', 'X', 'p', '<']

fig, axs = plt.subplots(1, figsize=(12,7))
axs.plot(x, linestyle='None', markersize=11)
for i, line in enumerate(axs.get_lines()):
    line.set_marker(markers[i])
axs.set_xlabel('Time', size=22)
axs.set_ylabel('Value', size=22)
axs.set_xticks([x for x in range(0, len(y))])              
axs.set_xticklabels(y, fontsize=18, rotation=15)           
axs.tick_params(axis='x',bottom=True,length=5)
axs.locator_params(axis='x', nbins=8)
axs.yaxis.set_tick_params(labelsize=18)
axs.legend(labels=process_tags, loc='upper right', fontsize=12)
axs.grid()
axs.hlines(0, xmin=0, xmax=550)
plt.show()
fig.savefig('unusual_measurements.pdf', bbox_inches='tight')


# =============================================================================
# XmR outside control limits
# =============================================================================
"""
Looping through the features and finding measurements that are outside the
control limits in the XmR charts. Storing the mesurements that are outside
as there original value and setting the measurements that are inside the 
control limits to None. This in order to plot only the measurements that 
indicate special variance. 
"""

X_out = []
mR_out = []

for feature in process_daily_std:
    x_out = []
    mr_out = []
    x_data = pd.Series(process_daily_std.loc[:, feature])
    x_data = x_data.reset_index(drop=True)
    x_mR_data = x_mR_calc(x_data, feature)
    x = x_mR_data['x'].values
    mR = x_mR_data['mR'].values
    
    ucl = statistics.mean(x)+3*statistics.mean(mR[1:len(mR)])/1.128
    lcl = statistics.mean(x)-3*statistics.mean(mR[1:len(mR)])/1.128
    
    ucl_mR = statistics.mean(mR[1:len(mR)])+3*statistics.mean(mR[1:len(mR)])*0.8525
    lcl_mR = statistics.mean(mR[1:len(mR)])-3*statistics.mean(mR[1:len(mR)])*0.8525
    
    for meas in x:
        if meas > ucl or meas < lcl:
            x_out.append(meas)
        else:
            x_out.append(None)
            
    X_out.append(x_out)
    
    for meas in mR:
        if meas > ucl_mR or meas < lcl_mR:
            mr_out.append(meas)
        else:
            mr_out.append(None)
            
    mR_out.append(mr_out)
    
        
# X chart    
# -----------------------------------------------------------------------------
process_daily_std_x_out = pd.DataFrame(X_out)
process_daily_std_x_out = pd.DataFrame(process_daily_std_x_out.T.values, 
                                       columns=process_tags, 
                                       index=process_daily.index)  


x2 = process_daily_std_x_out.values
y2 = process_daily_std_x_out.index.date  

# Plotting
fig, axs = plt.subplots(1, figsize=(15,10))
axs.plot(x2, 'o-', linestyle='None')
    
axs.set_xlabel('Time', size=22)
axs.set_ylabel('Value', size=22)
axs.set_xticks([x for x in range(0, len(y2))])              
axs.set_xticklabels(y, fontsize=18, rotation=15)           
axs.tick_params(axis='x',bottom=True,length=5)
axs.locator_params(axis='x', nbins=8)
axs.yaxis.set_tick_params(labelsize=18)
axs.legend(labels=process_tags, loc='lower right')
axs.grid()
axs.hlines(0, xmin=0, xmax=550)
plt.show()
# fig.savefig('X_outside.pdf', bbox_inches='tight')
  
    

# mR chart
# -----------------------------------------------------------------------------
process_daily_std_mR_out = pd.DataFrame(mR_out)
process_daily_std_mR_out = pd.DataFrame(process_daily_std_mR_out.T.values, 
                                        columns=process_tags, 
                                        index=process_daily.index)  


x3 = process_daily_std_mR_out.values
y3 = process_daily_std_mR_out.index.date


# Plotting
fig, axs = plt.subplots(1, figsize=(15,10))
axs.plot(x3, 'o-', linestyle='None')
    
axs.set_xlabel('Time', size=22)
axs.set_ylabel('Value', size=22)
axs.set_xticks([x for x in range(0, len(y3))])              
axs.set_xticklabels(y, fontsize=18, rotation=15)           
axs.tick_params(axis='x',bottom=True,length=5)
axs.locator_params(axis='x', nbins=8)
axs.yaxis.set_tick_params(labelsize=18)
axs.legend(labels=process_tags, loc='lower right')
axs.grid()
axs.hlines(0, xmin=0, xmax=550)
plt.show()
# fig.savefig('mR_outside.pdf', bbox_inches='tight')

